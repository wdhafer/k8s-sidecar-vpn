#!/usr/bin/env bash

# This is a convenience script to push the docker image to the docker hub
# repository.

for file in colors.sh logger.sh utils.sh config.sh; do
    [[ -f ${file} ]] && source ${file} || echo "Unable to load ${file}"
done

# Examples:
# docker login mycluster.icp:8500
# docker push mycluster.icp:8500/default/ace_bar:11.0.0.0

log "Pushing image with tag ${_Y}${tag}${_W}"
docker push ${tag}
