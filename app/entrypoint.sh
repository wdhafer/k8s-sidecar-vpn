#!/usr/bin/env bash

export USER_HOME=/openstack
export APP_HOME=${USER_HOME}/app

for file in colors.sh logger.sh utils.sh .rackspace; do
    [[ -f ${USER_HOME}/${file} ]] && source ${USER_HOME}/${file} || echo "Unable to load ${USER_HOME}/${file}"
done

log "Running entrypoint.sh..."
source .venv/bin/activate

#declare -r cloud="pubcloud" # should match a clouds.yaml entry
declare -r cloud="phobos" # value should match a clouds.yaml entry in your profile
declare -r region="DFW"
declare -r sleep_time="15"

while true; do
    if [[ "${cloud}" == "phobos" ]]; then
        log "Getting openstack image list for cloud ${cloud}..."
        openstack --os-cloud ${cloud} image list
    else
        log "Getting openstack image list for cloud ${cloud} in region ${region}..."
        openstack --os-cloud ${cloud} --os-region ${region} image list
    fi

    log "Sleeping for ${sleep_time} seconds..."
    sleep ${sleep_time}
done
