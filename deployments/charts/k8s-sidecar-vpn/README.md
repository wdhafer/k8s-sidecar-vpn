# Notes on deployment

## Minikube

If running locally, you can easily test with [minikube](https://github.com/kubernetes/minikube).

### Minikube Installation Mac

```bash
brew cask install minikube

# Make sure kubernetes-cli is also installed:
brew install kubernetes-cli
```

### Minikube Installation Linux

```bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
  && sudo install minikube-linux-amd64 /usr/local/bin/minikube
# Alternatively, see the other installation options on the github page:
# https://github.com/kubernetes/minikube
```

### Minikube Startup

```bash
# Start with default parameters
minikube start

# or with options
minikube start --cpus 8 --memory 12000

# a common env for this project would be:
minikube start --cpus 2 --memory 4000
```

### Minikube Shutdown

Issue the stop command to shutdown minikube:

```bash
minikube stop
```

## Helm Chart Installation

To install/deploy the helm chart, run the following (update with appropriate
values for the environment). This can be a bit tricky as all the values need
to be provided at run-time so that the configmap and secrets are created with
the appropriate values and correct locations.

```bash
# Make sure the namespace exists...in the following examples 'sidecar' is used
# for the namespace
kubectl create namespace sidecar

# Do this once per namespace
helm init

# Optionally, test the templates and other configs
helm lint

# Run the helm install with the '--dry-run' flag to test first - then remove
# the flag to actually deploy. The '--debug' flag renders the values.yaml
# and templates to STDOUT for debugging.  Use '--dry-run' and '--debug'
# together to quickly review and fix templating issues.
# Example helm install command with values redacted. Ideally this is
# ran from a CI/CD pipeline where the values are injected by the
# CI/CD service (Jenkins, Gitlab, etc.).
helm install \
    --debug \
    --namespace sidecar \
    --name k8s-sidecar-vpn \
    --set "app.image.tag=test" \
    --set "vpn.image.tag=test" \
    \
    --set "vpn.connection.gateway=<redacted>" \
    --set "vpn.connection.id=<redacted>" \
    --set "vpn.connection.secret=<redacted>" \
    --set "vpn.connection.username=<redacted>" \
    --set "vpn.connection.password=<redacted>" \
    \
    --set "clouds.pubcloud.project_name=<redacted>" \
    --set "clouds.pubcloud.username=<redacted>" \
    --set "clouds.pubcloud.password=<redacted>" \
    --set "clouds.phobos.project_name=<redacted>" \
    --set "clouds.phobos.project_id=<redacted>" \
    --set "clouds.phobos.username=<redacted>" \
    --set "clouds.phobos.password=<redacted>" \
    --set "clouds.phobos.user_domain_name=<redacted>" \
    --dry-run \
    .
```

View the output from the `--dry-run` to ensure the confgmaps and secrets have
all the appropriate values set.

To deploy a specific version of the containers, set the appropriate image tag
value.

```bash
# Specific version, set the [app|vpn].image.tag value - defaults are stored in
# the 'values.yaml' file
helm install \
    --namespace sidecar \
    --name k8s-sidecar-vpn \
    --set app.image.tag=0.1.0 \
    .

# Test (latest build) version, set the [app|vpn].image.tag value
helm install \
    --namespace sidecar \
    --name k8s-sidecar-vpn \
    --set app.image.tag=latest \
    .
```

## Uninstall

To uninstall the helm chart run:

```bash
helm delete k8s-sidecar-vpn --purge
```

## Viewing/Testing

The expected output is:

* **k8s-sidecar-app** - you should see a number of queries being performed with an image list returned from the OpenStack cluster
* **k8s-sidecar-vpn** - you should see the VPN client startup and run in the foreground without error

To view the logs of the App or VPN, run either of these commands:

```bash
# See the APP log
kubectl -n sidecar logs -f $(kubectl -n sidecar get pod -l component=k8s-sidecar-vpn -o json | jq -r ".items[] | .metadata.name") -c k8s-sidecar-app

# See the VPN log
kubectl -n sidecar logs -f $(kubectl -n sidecar get pod -l component=k8s-sidecar-vpn -o json | jq -r ".items[] | .metadata.name") -c k8s-sidecar-vpn
```

Also, monitoring the kubernetes resources via `watch` and `kubectl` can be
helpful:

```bash
watch kubectl get po,deploy,configmap,secrets -o wide --namespace sidecar
```
